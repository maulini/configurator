package connection.rabbitmq.impl;

public final class RabbitMQBuilder {

	//	private String user;
	//	private String password;
	//	private String routingKey;
	//	private String downRoutingKey;
	//	private String queue;
	//	private String exchangeName;
	//	private InetSocketAddress address;
	//
	//	public RabbitMQBuilder(InetSocketAddress address, String user, String password, String exchangeName) {
	//		this.address = address;
	//		this.user = user;
	//		this.password = password;
	//		this.exchangeName = exchangeName;
	//	}
	//
	//	public RabbitMQBuilder up(String routingKey) {
	//		this.routingKey = routingKey;
	//		return this;
	//	}
	//
	//	public RabbitMQBuilder down(String queue, String downRoutingKey) {
	//		this.queue = queue;
	//		this.downRoutingKey = downRoutingKey;
	//		return this;
	//	}
	//
	//	public IConnexion build() {
	//		return new Connexion();
	//	}
	//
	//	private class Connexion implements IConnexion {
	//
	//		private ReplaySubject<IMessage> subject = ReplaySubject.create();
	//		private ReplaySubject<Throwable> error = ReplaySubject.create();
	//		private Connection connection;
	//		private Channel channel;
	//
	//		@Override
	//		public void connection() {
	//			ConnectionFactory factory = new ConnectionFactory();
	//			factory.setHost(RabbitMQBuilder.this.address.getHostString());
	//			factory.setPort(RabbitMQBuilder.this.address.getPort());
	//			factory.setPassword(RabbitMQBuilder.this.password);
	//			factory.setUsername(RabbitMQBuilder.this.user);
	//			try {
	//				this.connection = factory.newConnection();
	//				this.channel = this.connection.createChannel();
	//				this.channel.exchangeDeclare(RabbitMQBuilder.this.exchangeName, "fanout");
	//				this.channel.queueDeclare(RabbitMQBuilder.this.queue, true, false, false, null);
	//				this.channel.queueBind(RabbitMQBuilder.this.queue, RabbitMQBuilder.this.exchangeName, RabbitMQBuilder.this.downRoutingKey);
	//			} catch (IOException | TimeoutException e) {
	//				this.error.onNext(e);
	//			}
	//		}
	//
	//		@Override
	//		public void send(IMessage message) {
	//			try {
	//				this.channel.basicPublish(RabbitMQBuilder.this.exchangeName, RabbitMQBuilder.this.routingKey, null, JsonFactory.encode(message, true).getBytes(Charset.forName("UTF-8")));
	//			} catch (IOException e) {
	//				this.error.onNext(e);
	//			}
	//		}
	//
	//		@Override
	//		public Observable<IMessage> downStream() {
	//			DeliverCallback deliverCallback = (consumerTag, delivery) -> {
	//				if(!delivery.getEnvelope().getRoutingKey().equals(RabbitMQBuilder.this.routingKey)) {
	//					String message = new String(delivery.getBody(), "UTF-8");
	//					IMessage messagin = JsonFactory.decode(message);
	//					this.subject.onNext(messagin);
	//				}
	//			};
	//			try {
	//				this.channel.basicConsume(RabbitMQBuilder.this.queue, true, deliverCallback, consumerTag -> { });
	//			} catch (IOException e) {
	//				this.error.onNext(e);
	//			}
	//			return this.subject;
	//		}
	//
	//		@Override
	//		public void close() {
	//			try {
	//				this.channel.close();
	//				this.connection.close();
	//			} catch (IOException | TimeoutException e) {
	//				this.error.onNext(e);
	//			}
	//		}
	//
	//		@Override
	//		public boolean isOpen() {
	//			return this.channel.isOpen() && this.connection.isOpen();
	//		}
	//
	//		@Override
	//		public Observable<Throwable> errorStream() {
	//			return this.error;
	//		}
	//
	//	}

}
