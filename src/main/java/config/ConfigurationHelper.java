package config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import com.typesafe.config.ConfigFactory;

public class ConfigurationHelper {

	private final File CONFIG_FILE;
	private final Map<String, String> configInFile = new HashMap<>();
	com.typesafe.config.Config conf;
	private String packag;

	public ConfigurationHelper(final String path, String packag) {
		this.CONFIG_FILE = new File(path);
		this.conf = ConfigFactory.parseFile(this.CONFIG_FILE);
		this.packag = packag;
	}

	public void getReflective(final Class<?>... clazz) {
		for (final Class<?> class1 : clazz) {
			for (final Field field : class1.getDeclaredFields()) {
				field.setAccessible(true);
				final Optional<Annotation> optional = Arrays.asList(field.getDeclaredAnnotations()).stream().filter(an -> an instanceof Config).findFirst();
				if(optional.isPresent()) {
					final String name = ((Config)optional.get()).value();
					this.injectValue(name, field);
				}
			}
		}

	}

	public void configure() {
		Reflections reflections = new Reflections(this.packag, new TypeAnnotationsScanner(), new FieldAnnotationsScanner());
		for (Field field : reflections.getFieldsAnnotatedWith(Config.class)) {
			if (field.getModifiers() == Modifier.STATIC + Modifier.PRIVATE || field.getModifiers() == Modifier.STATIC + Modifier.PUBLIC) {
				field.setAccessible(true);
				final Optional<Annotation> optional = Arrays.asList(field.getDeclaredAnnotations()).stream().filter(an -> an instanceof Config).findFirst();
				final String name = ((Config)optional.get()).value();
				this.injectValueWithConfig(name, field);
			}else {
				System.err.println("The field " + field.getName() + " isn't static");
			}
		}
	}

	private void injectValueWithConfig(String path, Field field) {
		try {
			final Class<?> clazz = field.getType();
			if (clazz == String.class) {
				field.set("", this.conf.getString(path));
			}else if(clazz == Integer.class || clazz == Integer.TYPE) {
				field.set(0, this.conf.getInt(path));
			}else if(clazz == Short.class || clazz == Short.TYPE) {
				field.setShort(0, (short) this.conf.getInt(path));
			}else if(clazz == Byte.class || clazz == Byte.TYPE) {
				field.setByte((byte)0, Byte.valueOf(this.conf.getString(path)));
			}else if(clazz == Double.class || clazz == Double.TYPE) {
				field.setDouble(0d, this.conf.getDouble(path));
			}else if(clazz == Float.class || clazz == Float.TYPE) {
				field.setFloat(0f, (float) this.conf.getDouble(path));
			}else if(clazz == Boolean.class || clazz == Boolean.TYPE) {
				field.setBoolean(true, this.conf.getBoolean(path));
			}else if(clazz == Long.class || clazz == Long.TYPE) {
				field.setLong(0L, this.conf.getLong(path));
			}
			System.err.println("Inject field : " + field.getName() + " / value : " + this.conf.getString(path));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			System.err.println("Erreur lors de l'injection");
			e.printStackTrace();
		}
	}

	private void getConfig() {
		try (BufferedReader br = new BufferedReader(new FileReader(this.CONFIG_FILE))){
			String ligne;
			while ((ligne = br.readLine()) != null) {
				final String[] tabs = ligne.trim().split(":");
				this.configInFile.put(tabs[0].trim(), tabs[1].trim());
			}
		} catch (final IOException e) {
			System.err.println("File Error");
			e.printStackTrace();
		}
	}

	private void injectValue(final String key, final Field field) {
		try {
			final String value = this.configInFile.get(key.trim());
			final Class<?> clazz = field.getType();
			if (clazz == String.class) {
				final String newValue = value.replace('"', ' ').trim();
				field.set("", newValue);
			}else if(clazz == Integer.class || clazz == Integer.TYPE) {
				field.set(0, Integer.valueOf(value));
			}else if(clazz == Short.class || clazz == Short.TYPE) {
				field.setShort(0, Short.valueOf(value));
			}else if(clazz == Byte.class || clazz == Byte.TYPE) {
				field.setByte((byte)0, Byte.valueOf(value));
			}else if(clazz == Double.class || clazz == Double.TYPE) {
				field.setDouble(0d, Double.valueOf(value));
			}else if(clazz == Float.class || clazz == Float.TYPE) {
				field.setFloat(0f, Float.valueOf(value));
			}else if(clazz == Boolean.class || clazz == Boolean.TYPE) {
				field.setBoolean(true, Boolean.valueOf(value));
			}else if(clazz == Long.class || clazz == Long.TYPE) {
				field.setLong(0L, Long.valueOf(value));
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			System.err.println("Erreur lors de l'injection");
			e.printStackTrace();
		}
	}
}
