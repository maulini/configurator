package config;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.inject.Binder;
import com.google.inject.MembersInjector;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import com.typesafe.config.Config;

import lombok.val;

@SuppressWarnings("rawtypes")
public class Configuration implements Module {


	protected final static Adapter FIELD_ADAPTER;
	private static final LinkedHashMap<Predicate<Class>, Adapter> ADAPTERS = new LinkedHashMap<Predicate<Class>, Configuration.Adapter>();


	static {
		ADAPTERS.put(String.class::equals, (config, configPath, fieldType) -> config.getString(configPath));

		ADAPTERS.put(InetAddress.class::isAssignableFrom, (config, configPath, fieldType) -> parseAddress(config.getString(configPath)));
		ADAPTERS.put(InetSocketAddress.class::isAssignableFrom, (config, configPath, fieldType) -> parseSocketAddress(config.getString(configPath)));

		ADAPTERS.put(Config.class::equals, (config, configPath, fieldType) -> config.getConfig(configPath));
		ADAPTERS.put(type -> Byte.class.equals(type) || Byte.TYPE.equals(type) || Short.class.equals(type) || Short.TYPE.equals(type) || Integer.class.equals(type) || Integer.TYPE.equals(type), (config, configPath, fieldType) -> config.getInt(configPath));
		ADAPTERS.put(type -> Long.class.equals(type) || Long.TYPE.equals(type), (config, configPath, fieldType) -> config.getLong(configPath));
		ADAPTERS.put(type -> Boolean.class.equals(type) || Boolean.TYPE.equals(type), (config, configPath, fieldType) -> config.getBoolean(configPath));
		ADAPTERS.put(type -> Float.class.equals(type) || Float.TYPE.equals(type) || Double.class.equals(type) || Double.TYPE.equals(type), (config, configPath, fieldType) -> config.getDouble(configPath));
		ADAPTERS.put(type -> List.class.equals(type) || Collection.class.equals(type) || Iterable.class.equals(type), (config, configPath, fieldType) -> config.getAnyRefList(configPath));
		ADAPTERS.put(Path.class::equals, (config, configPath, fieldType) -> Paths.get(config.getString(configPath)));
		ADAPTERS.put(Class::isArray, (config, configPath, fieldType) -> {
			val configArray = config.getAnyRefList(configPath);
			final Object array = Array.newInstance(fieldType.getComponentType(), configArray.size());
			for (int i = 0; i < configArray.size(); i++) {
				Array.set(array, i, configArray.get(i));
			}
			return array;
		});

		Adapter adapters = (config, configPath, fieldType) -> null;
		for (final Entry<Predicate<Class>, Adapter> entry : ADAPTERS.entrySet()) {
			adapters = linkAfter(entry.getKey(), adapters, entry.getValue());
		}
		FIELD_ADAPTER = adapters;
	}

	private final Config config;

	public Configuration(final Config config) {
		this.config = config;
	}

	@Override
	public void configure(final Binder binder) {
		binder.bindListener(Matchers.any(), listener((type, encounter) -> {
			final List<Injectee> injectees = Stream.of(type.getRawType().getDeclaredFields())
					.filter(x -> checkValidType(x.getType()))
					.filter(x -> x.isAnnotationPresent(config.Config.class))
					.filter(x -> !Modifier.isStatic(x.getModifiers()))
					.peek(x -> x.setAccessible(true))
					.map(x -> new Injectee(x, x.getAnnotation(config.Config.class).value()))
					.collect(Collectors.toList());
			encounter.register((MembersInjector<Object>) owner -> {
				for (final Injectee injectee : injectees) {
					try {
						val value = FIELD_ADAPTER.adapt(this.config, injectee.key, injectee.field.getType());
						if (value != null) {
							injectee.field.set(owner, value);
						}
					}catch (final Exception e) {
						binder.addError(e);
					}
				}
			});
		}));
		binder.bind(Config.class).toInstance(this.config);
	}

	private TypeListener listener(final BiConsumer<TypeLiteral<?>, TypeEncounter<?>> fn) {
		return fn::accept;
	}

	private static boolean checkValidType(final Class<?> clazz) {
		return clazz.isArray() && ADAPTERS.keySet().stream().anyMatch(p -> p.test(clazz.getComponentType())) || ADAPTERS.keySet().stream().anyMatch(p -> p.test(clazz));
	}

	static class Injectee {
		final Field field;
		final String key;

		public Injectee(final Field field, final String key) {
			this.field = Objects.requireNonNull(field);
			this.key = Objects.requireNonNull(key);
		}
	}

	interface Adapter {
		Object adapt(Config config, String configPath, Class fieldType);
	}

	static Adapter linkAfter(final Predicate<Class> afterIsCompatible, final Adapter before, final Adapter after) {
		return (config, configPath, fieldType) -> afterIsCompatible.test(fieldType) ? after.adapt(config, configPath, fieldType) : before.adapt(config, configPath, fieldType);
	}

	private static InetSocketAddress parseSocketAddress(final String address) {
		final String[] res = address.trim().split(":");
		if (res.length != 2) {
			throw new IllegalArgumentException(String.format("Address '%s' malformatted. Expected: 'x.x.x.x:port' or 'example.org:port'.", address));
		}
		try {
			return new InetSocketAddress(InetAddress.getByName(res[0]), Integer.parseInt(res[1]));
		} catch (NumberFormatException | UnknownHostException e) {
			throw new IllegalArgumentException(String.format("Address '%s' malformatted. Expected: 'x.x.x.x:port' or 'example.org:port'.", address));
		}
	}

	private static InetAddress parseAddress(final String address) {
		try {
			return InetAddress.getByName(address);
		} catch (final UnknownHostException e) {
			throw new IllegalArgumentException(String.format("Address '%s' malformatted. Expected: 'x.x.x.x:port' or 'example.org:port'.", address));
		}
	}


}
